import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductComponent } from './product/product.component';
import {InputModule} from "../../shared/components/input/input.module";
import {ButtonModule} from "../../shared/components/button/button.module";


@NgModule({
  declarations: [
    ProductsComponent,
    ProductComponent
  ],
  exports: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    InputModule,
    ButtonModule,
  ]
})
export class ProductsModule { }
